### Mavenfornewbies
tldr; I'm starting this project to help users better understand and use the [GitLab Maven Repository](https://docs.gitlab.com/ee/user/project/packages/maven_repository.html) 

### Overview
I'm Tim, the new PM for GitLab Package. Since starting, I've reviewed the backlog and seen all of the requests and issues with the product. One thing that struck me was that we didn't have a whole lot of content on how to use the Package features. So, I'm creating this project as a space for sample projects, templates, and things related to how to use the product. This particular project, as the name might sugggest, will focus on the Maven Repository. But please feel free to join the [GitLab Package](https://gitlab.com/gitlabpackage) group and see any of our other projects.  

### Goals
- As I train myself to use the product, share my progress and content with the GitLab community. 
- Create demonstratable projects that will allow me to better triage and support user requests.
- Utilize this sample project to test other package manager tools. 

### Helpful links
- [Apache Documentation](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html)
- [Jfrog Artifactory's Maven Documentation](https://www.jfrog.com/confluence/display/RTF/Maven+Repository)

